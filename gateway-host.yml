---
- hosts: gateway_hosts
  become: yes

  tasks:
  - import_tasks: vm-host.yml

  - name: Create data volume group on top of /dev/sda
    lvg:
      vg: data
      pvs: /dev/sda

  - name: Create Data Logical Volume (2 TB)
    lvol:
      vg: data
      lv: data
      size: "{{ volume_size }}"
    register: lvm_created

  - name: Check for previous install
    stat:
      path: /data/.gw_installed
    register: previous_runs

  - name: Format filesystem /dev/mapper/data
    filesystem:
      fstype: ext4
      dev: /dev/mapper/data-data
    when: not previous_runs.stat.exists and lvm_created.changed

  - name: Mount /data to new filesystem
    mount:
      path: /data
      src: /dev/mapper/data-data
      state: mounted
      fstype: ext4

  - name: Create new data directory
    file:
      path: /data/
      state: directory
      mode: '0775'
      owner: libvirt-qemu
      group: virt-admins    

  - name: Create new vms directory
    file:
      path: /data/vms
      state: directory
      mode: '0775'
      owner: libvirt-qemu
      group: virt-admins   
  
  - name: Create new iso directory
    file:
      path: /data/iso
      state: directory
      mode: '0775'
      owner: libvirt-qemu
      group: virt-admins

  - name: Open Firewall for VM Consoles
    ufw:
      state: enabled
      rule: allow
      port: 5900:5999
      proto: tcp

  - name: Configure tc for packet redirection to snoop on the host gateway's interface
    template:
      src: templates/mirror.j2
      dest: /etc/init.d/nids-port-mirroring
      owner: root
      group: root
      mode: 0755

  - name: Run packet snooping at rc5
    file:
      src: /etc/init.d/nids-port-mirroring
      dest: /etc/rc5.d/S01nids-port-mirroring
      owner: root
      group: root
      state: link

  - name: Create completion marker to prevent future destructive operations
    file:
      path: /data/.gw_installed
      state: touch
      mode: '0775'
      owner: libvirt-qemu
      group: virt-admins

  - name: Get VMs
    community.libvirt.virt:
      command: list_vms
    register: existing_vms_list

  - name: Check for previous VM configuration
    stat:
      path: /data/.vms_installed
    register: vm_runs

  - name: Check for previous ISO downloads (Security Onion)
    stat:
      path: /data/iso/securityonion-2.3.180-20221014.iso
    register: so_iso
    
  - name: Check for previous ISO downloads (pfSense)
    stat:
      path: /data/iso/pfSense-CE-2.5.1-RELEASE-amd64.iso
    register: pf_iso
  
  - name: Download Security Onion ISO
    get_url:
      url: https://download.securityonion.net/file/securityonion/securityonion-2.3.180-20221014.iso
      dest: /data/iso/securityonion-2.3.180-20221014.iso
      owner: libvirt-qemu
      group: virt-admins
    when: not so_iso.stat.exists

  - name: Download pfSense ISO
    get_url:
      url: https://atxfiles.netgate.com/mirror/downloads/pfSense-CE-2.5.1-RELEASE-amd64.iso.gz
      dest: /data/iso/pfSense-CE-2.5.1-RELEASE-amd64.iso.gz
      owner: libvirt-qemu
      group: virt-admins
    when: not pf_iso.stat.exists

  - name: Unarchive pfSense ISO
    command: /usr/bin/gunzip /data/iso/pfSense-CE-2.5.1-RELEASE-amd64.iso.gz 
    when: not pf_iso.stat.exists

  - name: Remove Compressed pfSense file
    command: /usr/bin/rm -f /data/iso/pfSense-CE-2.5.1-RELEASE-amd64.iso.gz
    when: not pf_iso.stat.exists

  - name: Unarchive pfSense ISO
    file:
      path: /data/iso/pfSense-CE-2.5.1-RELEASE-amd64.iso
      owner: libvirt-qemu
      group: virt-admins 
      mode: '0644'
    when: not pf_iso.stat.exists

  - name: Create Storage Pool (VMs)
    community.libvirt.virt_pool:
      command: define
      name: vms
      xml: '{{ lookup("template", "vms-datastore.xml.j2") }}'
      autostart: yes
    when: "so_hostname not in existing_vms_list.list_vms and not vm_runs.stat.exists"

  - name: Activate Storage Pool (VMs)
    community.libvirt.virt_pool:
      state: active
      name: vms

  - name: Create Storage Pool (ISOs)
    community.libvirt.virt_pool:
      command: define
      name: isos
      xml: '{{ lookup("template", "iso-datastore.xml.j2") }}'
      autostart: yes
    when: "so_hostname not in existing_vms_list.list_vms and not vm_runs.stat.exists"

  - name: Activate Storage Pool (ISOs)
    community.libvirt.virt_pool:
      state: active
      name: isos

  - name: Volume for Security Onion VM
    template:
      src: templates/so-storage.xml.j2
      dest: /tmp/so-storage.xml
      owner: libvirt-qemu
      group: virt-admins
      mode: 0640
  
  - name: Volume for pfSense VM
    template:
      src: templates/pf-storage.xml.j2
      dest: /tmp/pf-storage.xml
      owner: libvirt-qemu
      group: virt-admins
      mode: 0640

  - name: Manually create storage volume for Security Onion
    command: /usr/bin/virsh vol-create vms /tmp/so-storage.xml
    when: "so_hostname not in existing_vms_list.list_vms and not vm_runs.stat.exists"

  - name: Manually create storage volume for pfSense
    command: /usr/bin/virsh vol-create vms /tmp/pf-storage.xml
    when: "pf_hostname not in existing_vms_list.list_vms and not vm_runs.stat.exists"

  - name: Change owner of Security Onion Storage Volume
    file:
      path: "/data/vms/{{ so_hostname }}.qcow2"
      mode: '0660'
      owner: libvirt-qemu
      group: virt-admins

  - name: Change owner of pfSense Storage Volume
    file:
      path: "/data/vms/{{ pf_hostname }}.qcow2"
      mode: '0660'
      owner: libvirt-qemu
      group: virt-admins

  - name: Define Security Onion VM
    community.libvirt.virt:
      command: define
      name: "{{ so_hostname }}"
      autostart: yes
      xml: '{{ lookup("template", "so-config.xml.j2") }}'
    when: "so_hostname not in existing_vms_list.list_vms and not vm_runs.stat.exists"

  - name: Define pfSense VM
    community.libvirt.virt:
      command: define
      name: "{{ pf_hostname }}"
      autostart: yes
      xml: '{{ lookup("template", "pf-config.xml.j2") }}'
    when: "pf_hostname not in existing_vms_list.list_vms and not vm_runs.stat.exists"

  - name: Start Security Onion VM
    community.libvirt.virt:
      name: "{{ so_hostname }}"
      state: running

  - name: Start pfSense VM
    community.libvirt.virt:
      name: "{{ pf_hostname }}"
      state: running

  - name: Create VM completion marker to prevent future destructive operations
    file:
      path: /data/.vms_installed
      state: touch
      mode: '0775'
      owner: libvirt-qemu
      group: virt-admins

  - name: Initialize Aide Database
    pause:
      prompt: "Please run aide --init and remove the .new from the config file"
    when: not previous_runs.stat.exists