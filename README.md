# VM Hosting Servers
This is a set of playbooks that build various VM hosting servers.  It
uses a common underlying base playbook, and then a secondary one to configure
usage-specific settings such as firewall rules, storage configuration, etc.  You must have the 
libvirt ansible module installed to run this.  It
can be installed by executing:

```nsible-galaxy collection install community.libvirt```

## Gateway Security Applance Host (Ubuntu)
This playbook will take an Ubuntu 22.04 Server and configure it with
KVM/QEMU/Libvirtd to host virtual machines.  The express intent of this
configuration is to host two virtual machines: a firewall (pfSense - CE-2.5.1) and
the Security Onion (2.3.180) appliance.  This configuration assumes a second hard disk 
of 2 TB is installed, which will be configured via LVM to host the virtual machines.  
This playbook takes the below arguements:
* volume_size - (example: 1853g)
* tap_interface_name - (The network name of your TRUSTED interface - example: enp0s31f6)
* wan_interface_name - (The network name of your WAN interface - example: enx9cebe81fb16a)
* tap_destination_name - (example: virbr0)
* so_hostname - (example: security-onion)
* pf_hostname - (example: gateway-fw)

Example execution:

```ansible-playbook -u gateway-admin -k -K -e volume_size=1853g -e tap_interface_name=enp0s31f6 -e tap_destination_name=virbr0 -e so_hostname=sec-onion -e pf_hostname=gateway-fw -e wan_interface_name=enx9cebe81fb16a gateway-host.yml```

The playbook *should* be safe to re-run under any circumstances.  However, it's not throughly tested
so use at your own risk after the initial installation.

### Operational Validation
The host is configured to forward all traffic on the physical trusted interface 
to a private bridge, serving as a tap for Security Onion to perform IDS functions against.
Further details are available at https://backreference.org/2014/06/17/port-mirroring-with-linux-bridges/.  
To validate the tap is working you can run:

```systemctl status nids-port-mirroring```

Security Onion services can be checked via:

```so-status```

and access to resources is granted via:

```so-allow```

### Updates
Updates to Security Ontion is accomplished using the command-line update manager:

```sudo su && soup```

Updates to pfSense are accomplished via the UI.